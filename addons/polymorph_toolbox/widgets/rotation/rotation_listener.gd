extends Node

func _ready():
	$rotation.resize( Vector2(340,340) )
	$rotation.set_constrain( 0, Vector2( 0.3, 0.8 ) )
	$rotation.set_constrain( 1, Vector2( 0.3, 0.8 ) )
	_on_rotation_rotation_updated()

func _on_rotation_rotation_updated():
	$cub.rotation = $rotation.get_eulers()
	$cubx.rotation.x = $rotation.get_eulers().x
	$cuby.rotation.y = $rotation.get_eulers().y
	$cubz.rotation.z = $rotation.get_eulers().z