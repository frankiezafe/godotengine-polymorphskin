shader_type canvas_item;
render_mode blend_mix,unshaded;

uniform vec4 color_border;
uniform vec4 color_center;
uniform float center;
uniform float center_thickness;
uniform float gradient;
uniform float power;
uniform vec4 color_halo;
uniform float halo_center;
uniform float halo_size;
uniform float halo_power;
uniform float halo_stretch;
uniform float halo_strength;
uniform sampler2D noise_texture : hint_white;
uniform float noise_amount;
uniform vec2 uv_offset;
uniform vec2 uv_scale;

uniform float HALF_PI = 1.570796327;
uniform float PI = 3.141592653589;


void fragment(){
	
	vec2 f_uv = SCREEN_UV * uv_scale + uv_offset;
	vec4 noise_color = vec4( texture(noise_texture, f_uv).rgb, 1.0 );
	
	float ratio = SCREEN_PIXEL_SIZE.x / SCREEN_PIXEL_SIZE.y;
	
	float sy = max( 0.0, abs( ( 1.0 - center ) - SCREEN_UV.y ) - center_thickness );
	float d = ( 1.0 + sin( -HALF_PI + sy / gradient * PI ) ) * 0.5;
	float powd = pow( d, power );
	vec4 tmp = mix(color_center, color_border, powd );
	
	float pc = min( 1.0, sqrt( pow( ( halo_center - SCREEN_UV.x ) * mix( 1.0, powd, halo_stretch),  2 ) + pow( ( 1.0 - SCREEN_UV.y - center ) * ratio,  2 ) ) / halo_size );
	pc = 1.0 - ( 1.0 + sin( -HALF_PI + ( pc * PI ) ) ) * 0.5;
	pc = pow( pc, max( 0.5, halo_power ) );
	tmp = mix( tmp, color_halo, pc * halo_strength );
	
	COLOR = mix(tmp, noise_color, noise_amount );
	
}