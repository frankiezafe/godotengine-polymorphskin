#!/bin/bash

# newest godot version requires >-= python 3.5
# $ sudo apt install python3-venv
# python --version
# should output Python 2.x
# $ python3 -m venv myenv
# $ . ./myenv/bin/activate
# $ type python
# $ python --version
# should output Python 3.x

# removal of previous installation
cd ../
rm -rf godot

# getting godot engine
git clone git@github.com:godotengine/godot.git godot

# recompilation
cd godot/
git checkout master
scons platform=x11 -j 8
cd ..

# installing gtoolbox
cd gtoolbox
python install.py

# getting all polymorph modules
cd ../godot/modules

## getting gosc module
git clone git@gitlab.com:frankiezafe/gosc.git gosc
### running the installation of gosc
cd gosc
python install.py
cd ../

## getting softskin module
git clone git@gitlab.com:frankiezafe/SoftSkin.git softskin
### running the installation of gosc
cd softskin
python install.py
cd ../

## getting futari module
git clone git@gitlab.com:polymorphcool/futari-addon.git futari
### running the installation of gosc
cd futari
python install.py
cd ../

## getting gsyphon module
git clone git@gitlab.com:frankiezafe/gsyphon.git gsyphon
### running the installation of gsyphon
cd gsyphon
python install.py
cd ../

# back to godot
cd ../

# recompilation
scons platform=x11 -j 8 --config=force
# engine is recompiled with all module
